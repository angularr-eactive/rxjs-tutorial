import { Observable, fromEvent } from 'rxjs';
import { pluck, debounceTime, tap, map, defaultIfEmpty, mergeMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

const searchBox = document.querySelector('#search');
const results = document.querySelector('#results');
const count = document.querySelector('#count');

const URL = 'https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&utf8=1&srsearch=';

const search$ = fromEvent(searchBox, 'keyup').pipe(
    pluck('target', 'value'),
    debounceTime(500),
    tap(term => console.log(`Searching with term ${term}`)),
    map(query => URL + query),
    mergeMap(query => ajax({
        url: query, 
        method: 'GET', 
        headers: {'Access-Control-Allow-Origin': '*'}}).pipe(
        pluck('response', 'query', 'search'),
        defaultIfEmpty([])
    ))
);

search$.subscribe(console.log);